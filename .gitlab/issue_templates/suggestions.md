# Suggestion
(How could https://docs.nersc.gov be improved? Please provide a detailed summary of the issue and proposed solution)

## Relevant pages
(Please reference list of pages that need to be addressed.)

## workflow / steps to reproduce
(Please describe how you discovered this issue. e.g. Did you arrive on a page via google search, external link, slides, publication, talks, videos) 

## I confirm there is no existing [issue](https://gitlab.com/NERSC/nersc.gitlab.io/-/issues)?

- [ ] I confirm 

## Would you be willing to address this issue by [contributing](https://gitlab.com/NERSC/nersc.gitlab.io/-/blob/main/CONTRIBUTING.md) to NERSC documentation?

- [ ] Yes
- [ ] No

/label ~triage
