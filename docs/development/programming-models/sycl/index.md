# SYCL

SYCL is a cross-platform abstraction layer that enables code for
heterogeneous processors to be written using C++ with the host and
kernel code for an application contained in the same source file.

## Nightly builds

Nightly builds of git@github.com:intel/llvm.git are available as
modules on Perlmutter.

!!! tip
    These modules are also configured to support OpenMP offload on
    A100!

!!! warning
    A100 support is under [active
    developement](https://www.nersc.gov/news-publications/nersc-news/nersc-center-news/2021/nersc-alcf-codeplay-partner-on-sycl-for-next-generation-supercomputers/)!

```
module use /global/cfs/projectdirs/nstaff/cookbg/pe/shasta-21.11/modulefiles/core
module avail llvm/nightly
```

## Vector Addition Example

`main.cpp`

```cpp
--8<-- "docs/development/programming-models/sycl/main.cpp"
```

`Makefile`

```make
--8<-- "docs/development/programming-models/sycl/Makefile"
```

```console
$ module purge
$ module use /global/cfs/projectdirs/nstaff/cookbg/pe/shasta-21.11/modulefiles/core
$ module load llvm/nightly/latest
$ make
clang++ -std=c++17 -fsycl -fsycl-targets=nvptx64-nvidia-cuda -Xsycl-target-backend '--cuda-gpu-arch=sm_80' -o sycl-vecadd-buffer.x main.cpp
$ ./sycl-vecadd-buffer.x
sum = 1
```

## References

* [NERSC, ALCF, Codeplay partnership](https://www.nersc.gov/news-publications/nersc-news/nersc-center-news/2021/nersc-alcf-codeplay-partner-on-sycl-for-next-generation-supercomputers/)
* [DPC++ tutorial](https://github.com/jeffhammond/dpcpp-tutorial)
* [DPC++ Examples from Intel](https://www.intel.com/content/www/us/en/develop/documentation/explore-dpcpp-samples-from-intel/top.html)
* [Free ebook on SYCL programming](https://link.springer.com/book/10.1007/978-1-4842-5574-2)
* [SYCL 2020 Specification](https://www.khronos.org/registry/SYCL/specs/sycl-2020/html/sycl-2020.html)

## Support

* `#sycl` channel in [NERSC Users
  Slack](https://www.nersc.gov/users/NUG/nersc-users-slack/) (login
  required)
* [NERSC Help Desk](https://help.nersc.gov)
