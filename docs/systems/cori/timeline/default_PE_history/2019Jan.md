# Programming Environment Change on Cori and Edison in January 2019

## Background

Following the new Allocation Year start on January 8, 2019, Cori and
Edison will return to service with a new default programming
environment.

During the maintenance, we will change the default Cray Developer
Toolkit (CDT) version from
[18.03](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_18.03_March_1_2018/resource/Released_Cray_XC_Programming_Environments_18.03_March_1_2018.pdf)
to
[18.09](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_18.09_September_6_2018/resource/Released_Cray_XC_Programming_Environments_18.09_September_6_2018.pdf).
The old 
[CDT/18.06](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_18.06_June_7_2018/resource/Released_Cray_XC_Programming_Environments_18.06_June_7_2018.pdf)
will be removed. In addition, we will install a new release version,
[CDT/18.12](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_18.12_December_13_2018/resource/Released_Cray_XC_Programming_Environments_18.12_December_13_2018.pdf).

As a consequence of this change, the default versions of many packages
will change. The default version of the Intel compiler, however, will
remain the same.

Below is the detailed list of changes after AY2019 starts on January
8, 2019.

## Software default version changes

* atp/2.1.3 (from 2.1.1)
* cce/8.7.4 (from 8.6.5)
* cray-R/3.4.2 (from 3.3.3)
* cray-ccdb/3.0.4 (from 3.0.3)
* cray-cti/1.0.7 (from 1.0.6)
* cray-fftw/3.3.8.1 (from 3.3.6.3)
* cray-ga/5.3.0.8 (5.3.0.7)
* cray-hdf5, cray-hdf5-parallel/1.10.2.0 (from 1.10.1.1)
* cray-lgdb/3.0.10 (from 3.0.7)
* cray-libsci/18.07.1 (from 18.03.1)
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.3 (from 7.7.0)
* cray-netcdf, cray-netcdf-hdf5parallel/4.6.1.3 (from 4.4.1.1.6)
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.8.4.0 (from 3.7.6.2)
* cray-python/2.7.15.1 (from 2.7.13.1)
* cray-tpsl, cray-tpsl-64/18.06.1 (from 17.11.1)
* cray-trilinos/12.12.1.1 (from 12.10.1.2)
* craype/2.5.15 (from 2.5.14)
* craypkg-gen/1.3.7 (from 1.3.6)
* gdb4hpc/3.0.10	
* papi/5.6.0.3 (from 5.5.1.4)	
* perftools, perftools-base, perftools-lite/7.0.3 (from 7.0.0)
* pmi, pmi-lib/5.0.14 (from 5.0.13)
* stat/3.0.1.2 (from 3.0.1.1)

## Software versions to be removed

* atp/2.1.2
* cce/8.7.1
* cray-R/3.3.3
* cray-fftw/3.3.6.5
* cray-lgdb/3.0.9
* cray-libsci/18.04.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.1
* cray-netcdf, cray-netcdf-hdf5parallel/4.6.1.0
* cray-python/2.7.13.1
* cray-trilinos/12.12.1.0
* gdb4hpc/3.0.9
* papi/5.6.0.2
* perftools, perftools-base, perftools-lite/7.0.2

## New software versions available

* atp/2.1.2
* cce/8.7.7
* cray-ga/5.3.0.9
* cray-libsci/18.12.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.4
* cray-parallel-netcdf/1.8.1.4
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.9.3.0
* cray-python/2.7.15.3
* craype/2.5.16
* gcc/8.2.0
* modules/3.2.11.1
* papi/5.6.0.5
* perftools, perftools-base, perftools-lite/7.0.5
* valgrind4hpc/1.0.0
